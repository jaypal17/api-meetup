# This file demonstrates a connection to the Meet RSVP API for 60 seconds.
# This file is not meant to be a final product but to show connection and retrieval of data for 60 seconds.

import requests
import json
import threading

response = requests.get('http://stream.meetup.com/2/rsvps', stream=True)

# Time limit for 60 seconds of connection to API
timer = threading.Timer(60, response.close)

try:
    # Start counting
    timer.start()
    # Itering through data by line
    for data in response.iter_lines(chunk_size=100):

        if data:
            # Loads into a json format
            all_data = json.loads(data)
            # Print all data gathered in 60 seconds
            print(all_data)

except Exception as e:
    print("catch this", e)

finally:
    timer.cancel()
