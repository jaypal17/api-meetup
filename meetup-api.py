import requests
import json
import threading

response = requests.get('http://stream.meetup.com/2/rsvps', stream=True)
# Time limit for 60 seconds of connection to API
timer = threading.Timer(8, response.close)
try:
    # Start counting
    timer.start()
    # Number of RSVPs stored
    rsvp_total = 0
    # Date of Event furthest into the future
    date_future = {}
    # URL for furthest event
    future_link = {}
    # All Countries
    all_countries = {}
    # Top 3 Countries
    country_one = {}
    country_two = {}
    country_three = {}

    # Itering through data by line
    for data in response.iter_lines(chunk_size=100):

        if data:
            # Loads all data into Json format
            all_data = json.loads(data)
            # Counts RSVPs
            rsvp_total = rsvp_total + 1
            # Farthest RSVP Date
            # Check all data
            for all_data in response:
                # Check if date_future, and future_link does not have a value.
                if all_data["time"] not in date_future:
                    # If no value found add date and link
                    date_future.update(all_data["time"])
                    future_link.update(all_data["event_url"])
                # If date_future has value, compare next value. If next value is greater update date and link
                if all_data["time"] > date_future:
                    date_future.update(all_data["time"])
                    future_link.update(all_data["event_url"])
                # Check if country is in all_countries
                if all_data["group_country"] not in all_countries:
                    # Update country to all_countries
                    all_countries.update(all_data["group_country"])
                # Check if country is repeated and if so add to country 1,2, or 3.
                if all_data["group_country"] in all_countries:
                    # Add to country 1
                    country_one["group_country"].update(all_data["group_country"]) + 1
                    if all_data["group_country"] not in country_one:
                        if all_data["group_country"] > country_one:
                            country_one.update(all_data["group_country"])
                    if all_data["group_country"] in country_one:
                        country_one.update(all_data["group_country"])
                    # Update Country 2
                    if all_data["group_country"] not in country_two:
                        if all_data["group_country"] > country_two:
                            country_two.update(all_data["group_country"])
                    if all_data["group_country"] in country_two:
                        country_two.update(all_data["group_country"])
                    # Update Country 3
                    if all_data["group_country"] not in country_three:
                        if all_data["group_country"] > country_three:
                            country_three.update(all_data["group_country"])
                    if all_data["group_country"] in country_three:
                        country_three.update(all_data["group_country"])
# Print required info
    print(rsvp_total, date_future, future_link, country_one, country_two, country_three)
# For error catching
except Exception as e:
    print("catch this", e)
# End timer
finally:
    timer.cancel()
